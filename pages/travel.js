import Head from 'next/head';
import dynamic from 'next/dynamic';

const DynamicComponent = dynamic(() => import ('../components/StreetNavigation'))
// kaya gumamit ng dynamic component kasi di mababasa yung mapContainer if we don't

export default function travel(){
	return(
		<React.Fragment>
			<Head>
				<title>Book a ride</title>
			</Head>
			<DynamicComponent />
		</React.Fragment>
	)
}